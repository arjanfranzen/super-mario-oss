# What is Super Mario OSS?
Super Mario OSS is a clone of the original Super Mario Bros 1985 NES.
It is written in Python using pygame. The game is able to run on all systems 
that supports Python and pygame.

## Getting started
Go to requirements.txt and install the required packages using the following command:

![Install_pygame.png](img/Install_pygame.png)

# How to run the game
To run the game, you can use the following command:

![run_mario.png](img/run_mario.png)

it works!
![mario_game.png](img/mario_game.png)

# Original Project page.
https://sourceforge.net/projects/supermariobrosp/

# License
This file is part of Super Mario Bros. Python.

Super Mario Bros. Python is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Super Mario Bros. Python is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Super Mario Bros. Python.  If not, see <http://www.gnu.org/licenses/>.

                Copyright (C) 2009